package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.Product;
import com.ua.pt.FarmacySide.Entities.Status;
import com.ua.pt.FarmacySide.Entities.Status_Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepository extends CrudRepository<Status, Integer> {

    @Query(
            value = "SELECT * FROM status WHERE status.status_ID = ?1",
            nativeQuery = true
    )
    Status findStatusByStatus_ID(long id);

    @Query(
            value = "SELECT * FROM status WHERE status.status_info = ?1",
            nativeQuery = true
    )
    Status findStatusByStatus_info(String info);
}
