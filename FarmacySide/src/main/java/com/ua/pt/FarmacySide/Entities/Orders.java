package com.ua.pt.FarmacySide.Entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name= "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long order_ID;

    @NotNull
    private float total_price;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch=FetchType.EAGER)
    @JoinTable(name="products_order",
            joinColumns={@JoinColumn(name="order_ID")},
            inverseJoinColumns={@JoinColumn(name="product_ID")})
    private List<Product> products;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_ID")
    private User user_ID;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "address_ID")
    private Address address;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order_ID", cascade = CascadeType.REFRESH)
    private Set<Status_Order> status;

    public Orders() {}

    public Orders(float total_price, List<Product> products, User user_ID) {
        this.total_price = total_price;
        this.products = products;
        this.user_ID = user_ID;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public List<Product> getProducts() {

        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        this.total_price = 0;
        for(Product product : products) {
            this.total_price += product.getPrice();
        }
    }

    public User getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(User user_ID) {
        this.user_ID = user_ID;
    }

    public long getOrder_ID() {
        return order_ID;
    }

    public void setOrder_ID(long order_ID) {
        this.order_ID = order_ID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Status_Order> getStatus() {
        return status;
    }

    public void setStatus(Set<Status_Order> status) {
        this.status = status;
    }
}
