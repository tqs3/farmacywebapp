package com.ua.pt.FarmacySide.Entities;


import org.hibernate.annotations.Cascade;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "users")
public class User {

    public User() {
    }

    public User(String username, String email, String name, String password, String contact) {
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.contact = contact;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long user_ID;

    @NotNull
    private String username;

    @NotNull
    private String email;

    @NotNull
    private String name;

    @NotNull
    private String password;

    private String contact;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user_ID", cascade = CascadeType.REFRESH)
    private Set<Orders> orders;

    @OneToOne(mappedBy = "user")
    private Address address;


    public long getUser_ID() {
        return user_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Set<Orders> getOrders() {
        return orders;
    }

    public void setOrders(Set<Orders> orders) {
        this.orders = orders;
    }

    public void addOrder(Orders order) {
        this.orders.add(order);
    }

    public void setUser_ID(long user_ID) {
        this.user_ID = user_ID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
