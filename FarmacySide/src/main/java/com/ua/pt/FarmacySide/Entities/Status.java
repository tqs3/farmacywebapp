package com.ua.pt.FarmacySide.Entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long status_ID;

    @NotNull
    private String status_info;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "status_ID", cascade = CascadeType.REFRESH)
    private Set<Status_Order> orders;

    public Status(String status_info) {
        this.status_info = status_info;
    }

    public Status(){

    }

    public long getStatus_ID() {
        return status_ID;
    }

    public void setStatus_ID(long status_ID) {
        this.status_ID = status_ID;
    }

    public String getStatus_info() {
        return status_info;
    }

    public void setStatus_info(String status_info) {
        this.status_info = status_info;
    }

    public Set<Status_Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Status_Order> orders) {
        this.orders = orders;
    }
}
