package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    @Query(
            value = "SELECT * FROM products WHERE products.product_ID = ?1",
            nativeQuery = true
    )
    Product findProductByProduct_ID(long id);

    @Query(
            value = "SELECT product_ID FROM products ORDER BY product_ID DESC LIMIT 1",
            nativeQuery = true
    )
    long findIdLastProduct();



}
