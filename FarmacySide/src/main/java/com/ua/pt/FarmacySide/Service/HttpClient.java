package com.ua.pt.FarmacySide.Service;

import com.ua.pt.FarmacySide.Entities.Orders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class HttpClient {

    public JSONObject getResponse(String url) throws IOException {
        try(CloseableHttpClient session = HttpClients.createDefault();) {
            HttpGet request = new HttpGet(url);
            CloseableHttpResponse response = session.execute(request);
            try {
                HttpEntity responseData = response.getEntity();
                if (responseData != null) {
                    String data = EntityUtils.toString(responseData);
                    System.out.println("HTTP data " + data);
                    // parsing JSON
                    JSONParser parser = new JSONParser();
                    JSONObject result = (JSONObject) parser.parse(data);
                    System.out.println("HTTP result " + result);
                    return result;
                }
            } catch (Exception e) {
            } finally {
                if (response != null) {
                    response.close();
                    session.close();
                }
            }
        }
        return null;
    }

    public void postNewOrder(String url, Orders order) throws UnsupportedEncodingException {
        try(CloseableHttpClient session = HttpClients.createDefault();) {
            HttpPost request = new HttpPost(url);

            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("order_ID", String.valueOf(order.getOrder_ID())));
            urlParameters.add(new BasicNameValuePair("address", String.valueOf(order.getAddress().toString())));
            urlParameters.add(new BasicNameValuePair("store", "Rua Prof Carlos Almeida nº87, 5674-087, Aveiro, Aveiro, Portugal"));
            request.setEntity(new UrlEncodedFormEntity(urlParameters));
            try {
                CloseableHttpResponse response = session.execute(request);
                response.close();
                session.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
