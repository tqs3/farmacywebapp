package com.ua.pt.FarmacySide.Entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "products")
public class Product {

    public Product() {}

    public Product(String product_name, String info, int quantity, float price, String photo) {
        this.product_name = product_name;
        this.info = info;
        this.quantity = quantity;
        this.price = price;
        this.photo = photo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long product_ID;

    @NotNull
    public String product_name;

    @NotNull
    private String info;

    @NotNull
    private int quantity;

    @NotNull
    public float price;

    @NotNull
    public String photo;

    @ManyToMany(mappedBy="products")
    private Set<Orders> orders;



    public long getProduct_ID() {
        return product_ID;
    }

    public void setProduct_ID(long product_ID) {
        this.product_ID = product_ID;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Set<Orders> getOrders() {
        return orders;
    }

    public void setOrders(Set<Orders> orders) {
        this.orders = orders;
    }
}
