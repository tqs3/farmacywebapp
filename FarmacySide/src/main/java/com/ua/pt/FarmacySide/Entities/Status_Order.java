package com.ua.pt.FarmacySide.Entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name= "status_order")
public class Status_Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long entry_ID;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "order_ID")
    private Orders order_ID;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "status_ID")
    private Status status_ID;

    private LocalDateTime added_date;

    public Status_Order(Orders order_ID, Status status_ID, LocalDateTime added_date) {
        this.order_ID = order_ID;
        this.status_ID = status_ID;
        this.added_date = added_date;
    }

    public Status_Order(){

    }

    public long getEntry_ID() {
        return entry_ID;
    }

    public void setEntry_ID(long entry_ID) {
        this.entry_ID = entry_ID;
    }

    public Orders getOrder_ID() {
        return order_ID;
    }

    public void setOrder_ID(Orders order_ID) {
        this.order_ID = order_ID;
    }

    public Status getStatus_ID() {
        return status_ID;
    }

    public void setStatus_ID(Status status_ID) {
        this.status_ID = status_ID;
    }

    public LocalDateTime getAdded_date() {
        return added_date;
    }

    public void setAdded_date(LocalDateTime added_date) {
        this.added_date = added_date;
    }
}
