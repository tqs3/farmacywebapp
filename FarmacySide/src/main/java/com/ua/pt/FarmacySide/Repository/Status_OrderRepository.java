package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.Address;
import com.ua.pt.FarmacySide.Entities.Orders;
import com.ua.pt.FarmacySide.Entities.Status_Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Status_OrderRepository extends CrudRepository<Status_Order, Integer> {

    @Query(
            value = "SELECT * FROM status_order WHERE status_order.order_ID = ?1",
            nativeQuery = true
    )
    List<Status_Order> findStatus_OrderByOrder_ID(long id);
}
