package com.ua.pt.FarmacySide.Forms;

public class PaymentForm {
    public String cardnumber;
    public String month;
    public String year;
    public String cvc;
    public String ownername;

    public PaymentForm(String cardnumber, String month, String year, String cvc, String ownername) {
        this.cardnumber = cardnumber;
        this.month = month;
        this.year = year;
        this.cvc = cvc;
        this.ownername = ownername;
    }

    public PaymentForm(){

    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }
}
