package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.Orders;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository  extends CrudRepository<Orders, Integer> {
    @Query(
            value = "SELECT * FROM orders WHERE orders.user_ID = ?1",
            nativeQuery = true
    )
    List<Orders> findOrdersByUser_ID(long id);

}
