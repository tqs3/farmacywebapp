package com.ua.pt.FarmacySide.Forms;

public class SignUpForm {

    public String username;
    public String email;
    public String password1;
    public String password2;
    public String name;
    public String contact;


    public SignUpForm(String username, String email,String password1,String password2,String name, String contact ) {
        this.username = username;
        this.email=email;
        this.password1 = password1;
        this.password2=password2;
        this.name=name;
        this.contact=contact;
    }

    public SignUpForm() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
