package com.ua.pt.FarmacySide.Forms;

public class AddressForm {

    public String address;
    public int zipcode1;
    public int zipcode2;
    public String city;
    public String district;
    public String country;
    public int phonenumber;

    public AddressForm(String address, int zipcode1, int zipecode2, String city, String district, String country, int phonenumber) {
        this.address = address;
        this.zipcode1 = zipcode1;
        this.zipcode2 = zipecode2;
        this.city = city;
        this.district = district;
        this.country = country;
        this.phonenumber=phonenumber;
    }

    public AddressForm() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZipcode1() {
        return zipcode1;
    }

    public void setZipcode1(int zipcode1) {
        this.zipcode1 = zipcode1;
    }

    public int getZipcode2() {
        return zipcode2;
    }

    public void setZipcode2(int zipcode2) {
        this.zipcode2 = zipcode2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return "AddressForm{" +
                "address='" + address + '\'' +
                ", zipcode1=" + zipcode1 +
                ", zipcode2=" + zipcode2 +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
