package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.User;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserRepository extends CrudRepository<User, Integer> {

    User findUserByUsername(String username);

    @Query(
            value = "SELECT * FROM users WHERE users.user_ID = ?1",
            nativeQuery = true
    )
    User findUserByUser_ID(long id);

    @Query(
            value = "SELECT user_ID FROM users ORDER BY user_ID DESC LIMIT 1",
            nativeQuery = true
    )
    long findIdLastUser();


}
