package com.ua.pt.FarmacySide.Entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name= "addresses")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long address_ID;

    @NotNull
    private String address;

    @NotNull
    private int zipcode1;

    @NotNull
    private int zipcode2;

    @NotNull
    private String city;

    @NotNull
    private String district;

    @NotNull
    private String country;

    @NotNull
    private int phonenumber;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user")
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "address", cascade = CascadeType.ALL)
    private Set<Orders> orders;

    public Address(String address, int zipcode1, int zipcode2, String city, String district, String country, int phonenumber, User user) {
        this.address = address;
        this.zipcode1 = zipcode1;
        this.zipcode2 = zipcode2;
        this.city = city;
        this.district = district;
        this.country = country;
        this.phonenumber = phonenumber;
        this.user = user;
    }

    public Address() {

    }

    public long getAddress_ID() {
        return address_ID;
    }

    public void setAddress_ID(long address_ID) {
        this.address_ID = address_ID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZipcode1() {
        return zipcode1;
    }

    public void setZipcode1(int zipcode1) {
        this.zipcode1 = zipcode1;
    }

    public int getZipcode2() {
        return zipcode2;
    }

    public void setZipcode2(int zipcode2) {
        this.zipcode2 = zipcode2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Orders> getOrders() {
        return orders;
    }

    public void setOrders(Set<Orders> orders) {
        this.orders = orders;
    }
}
