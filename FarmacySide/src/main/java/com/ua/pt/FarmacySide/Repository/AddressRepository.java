package com.ua.pt.FarmacySide.Repository;

import com.ua.pt.FarmacySide.Entities.Address;
import com.ua.pt.FarmacySide.Entities.Orders;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Integer> {

}
