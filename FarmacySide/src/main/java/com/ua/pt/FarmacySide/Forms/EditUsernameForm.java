package com.ua.pt.FarmacySide.Forms;

public class EditUsernameForm {
    public String username;

    public EditUsernameForm(String username) {
        this.username = username;
    }
    public EditUsernameForm() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
