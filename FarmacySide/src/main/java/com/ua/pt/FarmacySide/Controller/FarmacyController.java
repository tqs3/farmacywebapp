package com.ua.pt.FarmacySide.Controller;

import com.ua.pt.FarmacySide.Entities.*;
import com.ua.pt.FarmacySide.Forms.*;
import com.ua.pt.FarmacySide.Repository.OrderRepository;
import com.ua.pt.FarmacySide.Repository.ProductRepository;
import com.ua.pt.FarmacySide.Service.FarmacyService;
import com.ua.pt.FarmacySide.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class FarmacyController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FarmacyService backend;

    private User actual_user = null;
    private Orders cart = null;
    private HashMap<Long, Integer> quantity = new HashMap<>();
    private PaymentForm paymentInfo = null;
    private AddressForm addressForm = null;

    private final String USER_ID = "USER_ID";
    private final String redirect_home = "redirect:/";
    private void getUserFromSession(HttpServletRequest request) {
        @SuppressWarnings("unchecked")
        Long user_ID = (Long) request.getSession().getAttribute(USER_ID);

        if (user_ID != null) {
            actual_user = backend.getUserByID(user_ID);
        } else {
            actual_user = null;
        }
    }

    @GetMapping("/")
    public ModelAndView welcome_page(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user != null) {
            return new ModelAndView("redirect:/home", model);
        } else {
            return new ModelAndView("index");
        }
    }

    @GetMapping("/login")
    public ModelAndView login_page(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user != null) {
            return new ModelAndView(redirect_home, model);
        }
        model.addAttribute("login_form", new LoginForm());
        return new ModelAndView("login");
    }

    @PostMapping("/login")
    public ModelAndView login_page(@ModelAttribute LoginForm login_form, ModelMap model, HttpServletRequest request) {

        String ckeckUser = backend.checkIfUserRegistered(login_form);

        if (ckeckUser.equals("doesnt exist")){
            model.addAttribute("username", true);
            model.addAttribute("password", false);
            model.addAttribute("login_form", new LoginForm());
            return new ModelAndView("login");
        } else if (ckeckUser.equals("password wrong")) {
            model.addAttribute("username", false);
            model.addAttribute("password", true);
            model.addAttribute("login_form", new LoginForm());
            return new ModelAndView("login");
        } else {
            actual_user = backend.fetchUser(login_form.getUsername());
            request.getSession().setAttribute(USER_ID, actual_user.getUser_ID());
            cart = backend.startCart(actual_user);
            return new ModelAndView(redirect_home, model);
        }
    }

    @GetMapping("/signup")
    public ModelAndView signup_page(ModelMap model, HttpServletRequest request) {
        model.addAttribute("signup_form", new SignUpForm());
        return new ModelAndView("signUp");
    }


    @PostMapping("/signup")
    public ModelAndView signup_page(@ModelAttribute SignUpForm signUpForm, ModelMap model, HttpServletRequest request){
        String infoUser = backend.saveUser(signUpForm);

        if (infoUser.equals("user save")){
            actual_user = backend.fetchUser(signUpForm.getUsername());
            return new ModelAndView(redirect_home, model);
        } else if (infoUser.equals("password do not match")) {

            model.addAttribute("password", true);
            model.addAttribute("username", false);

            model.addAttribute("signup_form", signUpForm);
            return new ModelAndView("signUp");
        } else {
            model.addAttribute("username", true);
            model.addAttribute("password", false);
            model.addAttribute("signup_form", signUpForm);
            return new ModelAndView("signUp");
        }


    }

    @GetMapping("/signout")
    public ModelAndView signout(ModelMap model, HttpServletRequest request) {
        if(actual_user != null) {
            actual_user = null;
            request.getSession().invalidate();
        }
        return new ModelAndView(redirect_home, model);
    }

    @GetMapping("/profile")
    public ModelAndView profile(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        model.addAttribute("username", actual_user.getUsername());
        model.addAttribute("email", actual_user.getEmail());
        model.addAttribute("name", actual_user.getName());
        model.addAttribute("contact", actual_user.getContact());
        model.addAttribute("user", actual_user);
        return new ModelAndView("profile");
    }

    @GetMapping("/profile/username")
    public ModelAndView editUsername(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        model.addAttribute("wrongUsername", false);
        model.addAttribute("editUsername_form", new EditUsernameForm());
        return new ModelAndView("editUsername");
    }

    @PostMapping("/profile/username")
    public ModelAndView editUsername(@ModelAttribute EditUsernameForm editUsernameForm, ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }

        boolean response = backend.changeUsername(editUsernameForm, actual_user);
        if (response) {
            actual_user.setUsername(editUsernameForm.getUsername());
            return new ModelAndView("redirect:/profile", model);
        } else {
            model.addAttribute("wrongUsername", true);
            model.addAttribute("editUsername_form", editUsernameForm);
            return new ModelAndView("editUsername");
        }
    }

    @GetMapping("/profile/email")
    public ModelAndView editEmail(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        model.addAttribute("wrongEmail", false);
        model.addAttribute("editEmail_form", new EditMailForm());
        return new ModelAndView("editEmail");
    }

    @PostMapping("/profile/email")
    public ModelAndView editEmail(@ModelAttribute EditMailForm editMailForm, ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }

        boolean response = backend.changeMail(editMailForm, actual_user);
        if (response) {
            actual_user.setEmail(editMailForm.getMail());
            return new ModelAndView("redirect:/profile", model);
        } else {
            model.addAttribute("wrongEmail", true);
            model.addAttribute("editEmail_form", editMailForm);
            return new ModelAndView("editEmail");
        }
    }

    @GetMapping("/profile/password")
    public ModelAndView editPassword(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        model.addAttribute("wrongPassword", false);
        model.addAttribute("editPassword_form", new EditPasswordForm());
        return new ModelAndView("editPassword");
    }

    @PostMapping("/profile/password")
    public ModelAndView editPassword(@ModelAttribute EditPasswordForm editPasswordForm, ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }

        boolean response = backend.changePassword(editPasswordForm, actual_user);
        if (response) {
            actual_user.setPassword(editPasswordForm.getPassword2());
            return new ModelAndView("redirect:/profile", model);
        } else {
            model.addAttribute("wrongPassword", true);
            model.addAttribute("editPassword_form", editPasswordForm);
            return new ModelAndView("editEmail");
        }
    }

    @GetMapping("/home")
    public ModelAndView home_page(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        List<Product> productList = backend.getAllProducts();
        model.addAttribute("productList", productList);
        return new ModelAndView("home");
    }

    @GetMapping("/home/{id}")
    public ModelAndView product_page(@PathVariable("id") long id ,ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if(actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        Product product = backend.getSpecificProduct(id);
        model.addAttribute("product",product);
        return new ModelAndView("productpage");
    }

    @GetMapping("/cart/{id}")
    public ModelAndView cart_page_add(@PathVariable("id") long id, ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        Product product = backend.getSpecificProduct(id);
        if (cart == null) {
            cart = backend.startCart(actual_user);
            cart = backend.updateCart(cart, product);
            quantity.put(product.getProduct_ID(), 1);
        } else {
            cart = backend.updateCart(cart, product);
            if (quantity.containsKey(product.getProduct_ID())) {
                Integer qtd = quantity.get(product.getProduct_ID());
                qtd = qtd +1;
                quantity.replace(product.getProduct_ID(), qtd);
            } else {
                quantity.put(product.getProduct_ID(), 1);
            }
        }
        return new ModelAndView("redirect:/cart");
    }

    @GetMapping("/cart/delete_quantity/{id}")
    public ModelAndView cart_page_delete_one_product(@PathVariable("id") long id, ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home, model);
        }
        Product product = backend.getSpecificProduct(id);
        if (cart != null) {
            cart = backend.deleteProductFromCart(cart, product);
            if (quantity.containsKey(product.getProduct_ID())) {
                Integer qtd = quantity.get(product.getProduct_ID());
                if(qtd == 1) {
                    quantity.remove(product.getProduct_ID());
                } else {
                    qtd = qtd - 1;
                    quantity.replace(product.getProduct_ID(), qtd);
                }
            }
        }
        return new ModelAndView("redirect:/cart");

    }

    @GetMapping("/cart")
    public ModelAndView cart_page(ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        if (cart == null) {
            cart = backend.startCart(actual_user);
        }
        List<Product> productList = backend.getUniqueListValues(cart.getProducts());
        model.addAttribute("products", productList);
        model.addAttribute("quantity", quantity);
        return new ModelAndView("cart");
    }

    @GetMapping("/cart/delete/{id}")
    public ModelAndView cart_page_delete(@PathVariable("id") long id, ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        Product product = backend.getSpecificProduct(id);
        cart = backend.deleteProductFromCart(cart, product);
        quantity.remove(product.getProduct_ID());
        return new ModelAndView("redirect:/cart");

    }

    @PostMapping("/confirm/address")
    public ModelAndView confirm_address(@ModelAttribute PaymentForm paymentForm, ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        if (cart == null) {
            return new ModelAndView("redirect:/home");
        }
        paymentInfo = paymentForm;
        model.addAttribute("address_form", new AddressForm());
        return new ModelAndView("address");
    }

    @PostMapping("/confirm/order")
    public ModelAndView confirm_order(@ModelAttribute AddressForm addressForm, ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        if (cart == null) {
            return new ModelAndView("redirect:/home");
        }
        this.addressForm = addressForm;
        actual_user = backend.confirmOrder(actual_user,addressForm, cart);
        cart = null;
        return new ModelAndView("redirect:/home");
    }

    @GetMapping("/confirm/payment")
    public ModelAndView confirm_payment(ModelMap model, HttpServletRequest request){
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        if (cart == null) {
            return new ModelAndView("redirect:/home");
        }
        model.addAttribute("payment_form", new PaymentForm());
        return new ModelAndView("payment");
    }

    @GetMapping("/orders")
    public ModelAndView follow_orders(ModelMap model, HttpServletRequest request) {
        this.getUserFromSession(request);
        if (actual_user == null) {
            return new ModelAndView(redirect_home);
        }
        HashMap<Orders, Status> orders = backend.get_status_of_orders(actual_user);
        model.addAttribute("orders", orders);
        return new ModelAndView("orders");
    }




    @RequestMapping(value = "/add" , method = RequestMethod.GET) // Map ONLY POST Requests
    public String addNewUser (@RequestParam(value = "username") String username ,@RequestParam(value = "name") String name
            , @RequestParam(value = "email") String email, @RequestParam(value = "password") String pass ) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        User n = new User();
        n.setUsername(username);
        n.setName(name);
        n.setEmail(email);
        n.setPassword(pass);
        userRepository.save(n);
        return "Saved";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Product> getAllUsers() {
        // This returns a JSON or XML with the users
        return productRepository.findAll();
    }
}
