package com.ua.pt.FarmacySide.Forms;

public class EditMailForm {
    public String mail;

    public EditMailForm(String mail) {
        this.mail = mail;
    }

    public EditMailForm() {
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
