package com.ua.pt.FarmacySide.Service;

import com.ua.pt.FarmacySide.Entities.*;
import com.ua.pt.FarmacySide.Forms.*;
import com.ua.pt.FarmacySide.Repository.*;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class FarmacyService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private Status_OrderRepository status_orderRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private HttpClient httpClient;


    public String checkIfUserRegistered(LoginForm loginForm) {
        User user = userRepository.findUserByUsername(loginForm.username);

        if(user == null) {
            return "doesnt exist";
        } else {
            if(user.getPassword().equals(loginForm.getPassword())){
                return "password right";
            } else {
                return "password wrong";
            }
        }
    }

    public User getUserByID(Long id) {
        return userRepository.findUserByUser_ID(id);
    }


    public String saveUser(SignUpForm signUpForm){

        User user = userRepository.findUserByUsername(signUpForm.getUsername());

        if(user == null) {
            if (signUpForm.getPassword1().equals(signUpForm.getPassword2())) {
                user = new User(signUpForm.getUsername(), signUpForm.getEmail(), signUpForm.getName(), signUpForm.getPassword1(), signUpForm.getContact());
                userRepository.save(user);
                return "user save";
            } else {
                return "password do not match";
            }
        } else {
            return "username used";
        }
    }

    public boolean changeUsername(EditUsernameForm editUsernameForm, User actual_user) {

        User user = userRepository.findUserByUsername(editUsernameForm.getUsername());
        if (user == null) {
            actual_user.setUsername(editUsernameForm.getUsername());
            userRepository.save(actual_user);
            return true;
        } else {
            return false;
        }
    }


    public boolean changeMail(EditMailForm editMailForm, User user) {

        Pattern pattern = Pattern.compile("[a-zA-Z0-9._-]*[@][a-zA-Z]*[.][a-z]{1,3}");
        Matcher matcher = pattern.matcher(editMailForm.getMail());
        boolean correct = matcher.find();
        if (correct) {
            user.setEmail(editMailForm.getMail());
            userRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    public boolean changePassword(EditPasswordForm editPasswordForm, User user) {

        if (user == null) {
            return false;
        }
        else {
            if(editPasswordForm.password1.equals(user.getPassword())){
                user.setPassword(editPasswordForm.getPassword2());
                userRepository.save(user);
                return true;

            }
           else {
               return false;
            }


        }
    }

    public List<Product> getAllProducts() {
        Iterable<Product> allProducts = productRepository.findAll();
        List<Product> productList = new ArrayList<Product>();

        for( Product p: allProducts){
            productList.add(p);
        }

        return productList;


    }

    public Product getSpecificProduct(long product_id){
        Product product = productRepository.findProductByProduct_ID(product_id);
        return product;

    }

    public User fetchUser(String username) {
        return userRepository.findUserByUsername(username);
    }

    public List<Product> getUniqueListValues(List<Product> productList) {

        List<Product> finalList = new ArrayList<>();
        List<Long> products_IDs = new ArrayList<>();
        Set<Long> ocurences = new HashSet<>();

        for (Product p: productList) {
            products_IDs.add(p.getProduct_ID());
        }

        for(int i = 0; i < products_IDs.size(); i++) {
            if (ocurences.contains(products_IDs.get(i))) {
            } else {
                ocurences.add(products_IDs.get(i));
                finalList.add(productList.get(i));
            }
        }

        return finalList;
    }

    public Orders startCart(User user) {
        Orders cart = new Orders(0, new ArrayList<>(), user);
        orderRepository.save(cart);
        return cart;
    }

    public Orders updateCart(Orders cart, Product product) {
        List<Product> products;
        if (cart.getProducts() == null) {
            products = new ArrayList<>();
        } else {
            products = cart.getProducts();
        }
        products.add(product);
        cart.setProducts(products);
        orderRepository.save(cart);
        return cart;
    }

    public Orders deleteProductFromCart(Orders cart, Product product) {
        List<Product> products = cart.getProducts();
        int i = 0;
        for( Product p : products) {
            if (p.getProduct_ID() == product.getProduct_ID()) {
                break;
            }
            i++;
        }
        products.remove(i);
        cart.setProducts(products);
        orderRepository.save(cart);
        return cart;
    }

    public User confirmOrder(User user, AddressForm addressForm, Orders cart) {
        Address address = new Address(addressForm.getAddress(),
                addressForm.getZipcode1(),
                addressForm.getZipcode2(),
                addressForm.getCity(),
                addressForm.getDistrict(),
                addressForm.getCountry(),
                addressForm.getPhonenumber(),
                user);
        user.setAddress(address);
        cart.setAddress(address);
        Set<Orders> orders;
        if (user.getOrders() == null) {
            orders = new HashSet<>();
        } else {
            orders = user.getOrders();
        }
        System.out.println(cart.getProducts());
        cart.setTotal_price(this.calculateOrderTotalPrice(cart.getProducts()));
        orders.add(cart);
        user.setOrders(orders);
        Status status = statusRepository.findStatusByStatus_ID(1);
        Status_Order status_order = new Status_Order(cart, status, LocalDateTime.now());
        addressRepository.save(address);
        orderRepository.save(cart);
        userRepository.save(user);
        status_orderRepository.save(status_order);
        return user;
    }

    public float calculateOrderTotalPrice(List<Product> products) {
        float total = 0;
        for(Product product : products) {
            total += product.getPrice();
        }
        return total;
    }

    public HashMap<Orders, Status> get_status_of_orders(User user) {
        HashMap<Orders, Status> orders_status = new HashMap<>();
        this.fetchStatusOfOrders(user.getUser_ID());
        List<Orders> orders = orderRepository.findOrdersByUser_ID(user.getUser_ID());
        for (Orders order: orders) {
            List<Status_Order> status = status_orderRepository.findStatus_OrderByOrder_ID(order.getOrder_ID());
            if (status.isEmpty()) {
                continue;
            }
            LocalDateTime recent_date = status.get(0).getAdded_date();
            Status_Order recent_status = status.get(0);
            for (Status_Order s : status) {
                if (s.getAdded_date().isAfter(recent_date)) {
                    recent_date = s.getAdded_date();
                    recent_status = s;
                }
            }
            orders_status.put(order, recent_status.getStatus_ID());
        }
        return orders_status;
    }

    public void fetchStatusOfOrders(long user_ID) {
        List<Orders> user_orders = orderRepository.findOrdersByUser_ID(user_ID);
        for(Orders order : user_orders) {
            try {
                JSONObject res = httpClient.getResponse("http://localhost:8086/order="+order.getOrder_ID());
                JSONObject data = (JSONObject) res.get("data");
                Set<Status_Order> list_of_status = order.getStatus();
                Status status = statusRepository.findStatusByStatus_info((String)data.get("status"));
                Status_Order status_order = new Status_Order(order, status, LocalDateTime.now());
                if (list_of_status == null) {
                    list_of_status = new HashSet<>();
                }
                list_of_status.add(status_order);
                order.setStatus(list_of_status);
                orderRepository.save(order);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendOrderToEstafetas(Orders cart) {
        try {
            httpClient.postNewOrder("http://localhost:8086/newOrder", cart);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


}
