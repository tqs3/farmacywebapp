/*ALTER USER 'root'@'localhost' IDENTIFIED BY 'farmacyTQSProject2021';*/

/*CREATE DATABASE farmacy;*/
/*CREATE USER 'springuser'@'%' IDENTIFIED BY 'SpringUser';*/
/*GRANT ALL ON farmacy.* TO 'springuser'@'%';*/

CREATE TABLE users (
    user_ID bigint(20) NOT NULL AUTO_INCREMENT,
    username varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    name varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    contact varchar(50) DEFAULT NULL,
    PRIMARY KEY (user_ID),
    UNIQUE KEY UK_username (username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE products (
    product_ID bigint(20) NOT NULL AUTO_INCREMENT,
    product_name varchar(50)  NOT NULL,
    info varchar(500) NOT NULL,
    quantity int NOT NULL,
    price float(50)  NOT NULL,
    photo varchar(50) NOT NULL,

    PRIMARY KEY (product_ID),
    UNIQUE KEY UK_name (product_name)
);
CREATE TABLE addresses(
    address_ID bigint(20) NOT NULL AUTO_INCREMENT,
    address varchar(300) NOT NULL,
    zipcode1 int NOT NULL ,
    zipcode2 int NOT NULL ,
    city varchar(50) NOT NULL ,
    district varchar(50) NOT NULL ,
    country varchar(50) NOT NULL,
    phonenumber int NOT NULL ,
    user bigint(20) NOT NULL,
    PRIMARY KEY (address_ID),
    FOREIGN KEY(user) REFERENCES users(user_ID)
);

CREATE TABLE orders(
    order_ID bigint(20) NOT NULL AUTO_INCREMENT,
    total_price float(50)  DEFAULT  0.0,
    user_ID bigint(20) NOT NULL,
    address_ID bigint(20),
    FOREIGN KEY(user_ID) REFERENCES users(user_ID),
    FOREIGN KEY(address_ID) REFERENCES addresses(address_ID),
    PRIMARY KEY(order_ID)
);

CREATE TABLE status(
    status_ID bigint(20) NOT NULL AUTO_INCREMENT,
    status_info varchar(30) NOT NULL,
    PRIMARY KEY(status_ID),
    UNIQUE KEY UK_username (status_info)
);


CREATE TABLE products_order(
  products_order_ID  bigint(20) NOT NULL AUTO_INCREMENT,
  order_ID bigint(20) NOT NULL ,
  product_ID  bigint(20) NOT NULL,
  PRIMARY KEY (products_order_ID),
  FOREIGN KEY (order_ID) REFERENCES orders(order_ID),
  FOREIGN KEY (product_ID) REFERENCES products(product_ID)

);

CREATE TABLE status_order(
  entry_ID bigint(20) NOT NULL AUTO_INCREMENT,
  order_ID bigint(20) NOT NULL,
  status_ID bigint(20) NOT NULL,
  added_date datetime NOT NULL,
  PRIMARY KEY (entry_ID),
  FOREIGN KEY (order_ID) REFERENCES orders(order_ID),
  FOREIGN KEY (status_ID) REFERENCES status(status_ID)
);







