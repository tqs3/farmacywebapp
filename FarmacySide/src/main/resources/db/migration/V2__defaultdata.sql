INSERT INTO users(username, email, name, password, contact)
VALUES ('mike123', 'mike@ua.pt', 'Miguel Marques', '1234', 912345678);

INSERT INTO users(username, email, name, password, contact)
VALUES ('trio123', 'trio@ua.pt', 'Trio Quaresma', '1234', 912345678);

INSERT INTO users(username, email, name, password, contact)
VALUES ('joao14', 'joao145@ua.pt', 'João Fernandes', '1234', 912345678);

INSERT INTO users(username, email, name, password, contact)
VALUES ('carlos125', 'carlos125@ua.pt', 'Carlos Mendonça', '1234', 912345678);


INSERT INTO status(status_info)
VALUES ('waiting');

INSERT INTO status(status_info)
VALUES ('accepted');

INSERT INTO status(status_info)
VALUES ('prepared to collect');

INSERT INTO status(status_info)
VALUES ('collected');

INSERT INTO status(status_info)
VALUES ('in transit');

INSERT INTO status(status_info)
VALUES ('delivered');


INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Benurom', 'Benurom 1000mg', 15, 5.76, '/db_images/benurom.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Brufen', 'Brufen 200mg', 20, 6.57, '/db_images/brufen.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Aerius', 'Aerius 5mg', 10, 2.46, '/db_images/aerius5mg.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Aerius Xarope', 'Aerius 0.5mg/150ml', 23, 7.56, '/db_images/aeriusXarope.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Bisoltussin', 'Bisoltussin 2mg/200ml', 12, 8.60, '/db_images/bisoltussin.png');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Fluimucil', 'Fluimucil 600mg', 15, 7.70, '/db_images/fluimucil.png');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Iburon', 'Iburon 150mg', 30, 4.50, '/db_images/iburon.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Aspirina C', 'Aspirina C 400mg', 35, 5.05, '/db_images/aspirina_c.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Voltaren Emulgel', 'Voltaren Emulgel 120g', 10, 11.95, '/db_images/voltaren_emulgel.jpeg');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Strepfen', 'Strepfen 24 pastilhas', 10, 6.60, '/db_images/strepfen.png');

INSERT INTO products(product_name, info, quantity, price, photo)
VALUES ('Cêgripe', 'Cêgripe 500mg', 20, 7.95, '/db_images/cegripe.png');





