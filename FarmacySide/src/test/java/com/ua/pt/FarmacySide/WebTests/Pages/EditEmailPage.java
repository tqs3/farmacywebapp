package com.ua.pt.FarmacySide.WebTests.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class EditEmailPage {

    private WebDriver driver;

    @FindBy(how = How.ID, using = "email")
    private WebElement emailField;

    @FindBy(how = How.CSS, using = "#btChange > b")
    private WebElement changeEmailButton;

    public EditEmailPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void insertEmailInfoInForm(String email) {
        emailField.click();
        emailField.sendKeys(email);
    }

    public void clickChangeEmailButton() {
        changeEmailButton.click();
    }


}
