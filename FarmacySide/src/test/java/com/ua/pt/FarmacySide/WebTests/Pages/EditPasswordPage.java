package com.ua.pt.FarmacySide.WebTests.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class EditPasswordPage {

    private WebDriver driver;

    @FindBy(how = How.ID, using = "psw1")
    private WebElement passwordFieldOld;

    @FindBy(how = How.ID, using = "psw2")
    private WebElement passwordFieldNew;

    @FindBy(how = How.ID, using = "btChange")
    private WebElement buttonChange;


    public EditPasswordPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void insertOldPassword(String oldPassword) {
        passwordFieldOld.click();
        passwordFieldOld.sendKeys(oldPassword);
    }

    public void insertNewPassword(String newPassword) {
        passwordFieldNew.click();
        passwordFieldNew.sendKeys(newPassword);
    }

    public void clickChangeButton() {
        buttonChange.click();
    }
}
