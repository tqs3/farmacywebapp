package com.ua.pt.FarmacySide.WebTests;// Generated by Selenium IDE

import com.ua.pt.FarmacySide.WebTests.Pages.*;
import io.github.bonigarcia.seljup.Options;
import io.github.bonigarcia.seljup.SeleniumJupiter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.*;
import java.util.concurrent.TimeUnit;

@ExtendWith(SeleniumJupiter.class)
public class ChangeEmailTest {
  private RemoteWebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;

  @Options
  FirefoxOptions firefoxOptions = new FirefoxOptions();
  {
    firefoxOptions.setHeadless(true);
  }

  @BeforeEach
  public void setUp(FirefoxDriver driver) {
    this.driver = driver;
    js = (JavascriptExecutor) this.driver;
    vars = new HashMap<String, Object>();
  }
  @AfterEach
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void changeEmail() {
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    WelcomePage welcomePage = new WelcomePage(driver);
    LoginPage loginPage = new LoginPage(driver);
    HomePage homePage = new HomePage(driver);
    ProfilePage profilePage = new ProfilePage(driver);
    EditEmailPage editEmailPage = new EditEmailPage(driver);

    welcomePage.clickLoginButton();

    loginPage.insertInfoInLoginForm("mike123", "1234");
    loginPage.clickLoginButton();

    homePage.clickProfilePicture();

    assertThat(profilePage.getEmailText(), is("mike@ua.pt"));
    profilePage.clickEditEmailButton();

    editEmailPage.insertEmailInfoInForm("m.garcia.marques@ua.pt");
    editEmailPage.clickChangeEmailButton();

    assertThat(profilePage.getEmailText(), is("m.garcia.marques@ua.pt"));
    profilePage.clickEditEmailButton();

    editEmailPage.insertEmailInfoInForm("mike@ua.pt");
    editEmailPage.clickChangeEmailButton();

    assertThat(profilePage.getEmailText(), is("mike@ua.pt"));
    profilePage.clickSignOutButton();


  }
}
