package com.ua.pt.FarmacySide.RepositoryTests;

import com.ua.pt.FarmacySide.Entities.User;
import com.ua.pt.FarmacySide.Repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    private User user1;
    private User user2;
    private User user3;
    private User user4;

    @BeforeEach
    public void setUp() {
        
        user1 = userRepository.save(new User(
                "mike321",
                "mike321@ua.pt",
                "Miguel Marques",
                "4321",
                "918467367"));

        user2 = userRepository.save(new User(
                "peter523",
                "peter523@ua.pt",
                "Peter Quaresma",
                "4321",
                "918467367"));

        user3 = userRepository.save(new User(
                "anne54",
                "anne54@ua.pt",
                "Anna Roterdan",
                "4321",
                "918467367"));

        user4 = userRepository.save(new User(
                "joanne98",
                "joanne98@ua.pt",
                "Joanne Fatul",
                "4321",
                "918467367"));

    }

    @AfterEach
    public void tearDown() {
        userRepository.delete(user1);
        userRepository.delete(user2);
        userRepository.delete(user3);
        userRepository.delete(user4);
    }

    @Test
    public void test_askUserByUsername() {
        User result = userRepository.findUserByUsername("mike321");

        assertThat(result).isEqualTo(user1);
    }

    @Test
    public void test_askUserByID() {
        long ID_last_user = userRepository.findIdLastUser();
        User result = userRepository.findUserByUser_ID(ID_last_user);

        assertThat(result.getUser_ID()).isEqualTo(user4.getUser_ID());

        result = userRepository.findUserByUser_ID(ID_last_user - 1);

        assertThat(result.getUser_ID()).isEqualTo(user3.getUser_ID());

        result = userRepository.findUserByUser_ID(ID_last_user - 2);

        assertThat(result.getUser_ID()).isEqualTo(user2.getUser_ID());

        result = userRepository.findUserByUser_ID(ID_last_user - 3);

        assertThat(result.getUser_ID()).isEqualTo(user1.getUser_ID());
    }
}
