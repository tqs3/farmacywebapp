package com.ua.pt.FarmacySide.ServiceTests;

import com.ua.pt.FarmacySide.Entities.Address;
import com.ua.pt.FarmacySide.Entities.Orders;
import com.ua.pt.FarmacySide.Entities.Product;
import com.ua.pt.FarmacySide.Entities.User;
import com.ua.pt.FarmacySide.Repository.AddressRepository;
import com.ua.pt.FarmacySide.Repository.OrderRepository;
import com.ua.pt.FarmacySide.Repository.ProductRepository;
import com.ua.pt.FarmacySide.Repository.UserRepository;
import com.ua.pt.FarmacySide.Service.FarmacyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class OrderRelated_Service_UnitTests {

    @Mock
    private OrderRepository orderRepository;


    @InjectMocks
    private FarmacyService service;

    private User user1;

    private Product product1;
    private Product product2;

    private Address address1;

    private List<Product> productList;

    private Orders order1;

    @BeforeEach
    public void setUp() {
        user1 = new User(
                "mike321",
                "mike321@ua.pt",
                "Miguel Marques",
                "4321",
                "918467367");

        product1 =new Product(
                "Benurom 1000",
                "Benurom 1000mg",
                15,
                (float) 5.76,
                "/db_images/benurom.jpeg"
        );

        product2 = new Product(
                "Aerius 5mg",
                "Aerius 5mg",
                10,
                (float) 2.46,
                "/db_images/aerius5mg.jpeg"
        );

        address1 = new Address(
                "Rua Prof José Carvalho nº870",
                3870,
                067,
                "Aveiro",
                "Aveiro",
                "Portugal",
                918567487,
                user1
        );

        productList = new ArrayList<>();
        productList.add(product1);

        order1 = new Orders(0, new ArrayList<>(), user1);

        order1.setProducts(productList);
        order1.setAddress(address1);
    }

    @Test
    public void testIfTheCartIsStartedAndSavedCorrectly() {
        Orders order = service.startCart(user1);
        verify(orderRepository, times(1)).save(order);
        assertThat(order.getUser_ID().getUser_ID()).isEqualTo(user1.getUser_ID());
    }

    @Test
    public void testIfProductIsAddedCorrectly() {
        Orders response = service.updateCart(order1, product2);
        verify(orderRepository, times(1)).save(order1);
        productList.add(product2);
        assertThat(response.getProducts()).isEqualTo(productList);
    }



}
