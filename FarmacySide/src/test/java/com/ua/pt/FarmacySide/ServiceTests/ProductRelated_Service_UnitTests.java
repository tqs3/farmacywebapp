package com.ua.pt.FarmacySide.ServiceTests;

import com.ua.pt.FarmacySide.Entities.Product;
import com.ua.pt.FarmacySide.Repository.ProductRepository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import com.ua.pt.FarmacySide.Service.FarmacyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ProductRelated_Service_UnitTests {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private FarmacyService service;

    List<Product> productList ;

    Product product1;
    Product product2;

    @BeforeEach
    public void setUp() {

        productList = new ArrayList<>();

        product1 = new Product(
                "Benurom 1000",
                "Benurom 1000mg",
                15,
                (float) 5.76,
                "/db_images/benurom.jpeg"
        );

        product2 = new Product(
                "Aerius 5mg",
                "Aerius 5mg",
                10,
                (float) 2.46,
                "/db_images/aerius5mg.jpeg"
        );

        product1.setProduct_ID(1);
        product2.setProduct_ID(2);

        productList.add(product1);
        productList.add(product1);
        productList.add(product2);
        productList.add(product2);
    }

    @Test
    public void testIfMethod_getUniqueListValues_returnsOnlyTheUniqueOnes() {
        List<Product> response = service.getUniqueListValues(productList);

        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void testIfMethod_getAllProducts_returnsTheCorrectListOfProducts() {
        Iterable<Product> products = productList;
        given(productRepository.findAll()).willReturn(products);
        List<Product> response = service.getAllProducts();

        assertThat(response).isEqualTo(productList);
    }

    @Test
    public void testIfMethod_getSpecificProduct_returnsTheCorrectProduct() {
        given(productRepository.findProductByProduct_ID(any(Long.class))).willReturn(product1);
        Product response = service.getSpecificProduct(2);
        assertThat(response).isEqualTo(product1);
    }
}
