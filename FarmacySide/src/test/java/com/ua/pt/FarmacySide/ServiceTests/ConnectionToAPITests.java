package com.ua.pt.FarmacySide.ServiceTests;

import com.ua.pt.FarmacySide.Entities.*;
import com.ua.pt.FarmacySide.Repository.OrderRepository;
import com.ua.pt.FarmacySide.Repository.StatusRepository;
import com.ua.pt.FarmacySide.Service.FarmacyService;
import com.ua.pt.FarmacySide.Service.HttpClient;
import io.cucumber.java.sl.In;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ConnectionToAPITests {

    @Mock
    private HttpClient httpClient;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private StatusRepository statusRepository;

    @InjectMocks
    private FarmacyService service;

    private JSONObject response1;
    private JSONObject response2;

    private Orders order1;
    private Orders order2;

    private User user1;

    private Product product1;
    private Product product2;

    private List<Product> productList1;
    private List<Product> productList2;

    private Address address1;

    private List<Orders> listOrders;

    private Status status1;
    private Status status2;

    @BeforeEach
    public void setUp() {
        user1 = new User(
                "mike321",
                "mike321@ua.pt",
                "Miguel Marques",
                "4321",
                "918467367");

        product1 = new Product(
                "Benurom 1000",
                "Benurom 1000mg",
                15,
                (float) 5.76,
                "/db_images/benurom.jpeg"
        );

        product2 = new Product(
                "Aerius 5mg",
                "Aerius 5mg",
                10,
                (float) 2.46,
                "/db_images/aerius5mg.jpeg"
        );

        address1 = new Address(
                "Rua Prof José Carvalho nº870",
                3870,
                067,
                "Aveiro",
                "Aveiro",
                "Portugal",
                918567487,
                user1
        );

        productList1 = new ArrayList<>();
        productList2 = new ArrayList<>();
        productList1.add(product1);
        productList2.add(product2);

        order1 = new Orders(0, new ArrayList<>(), user1);
        order2 = new Orders(0, new ArrayList<>(), user1);

        order1.setProducts(productList1);
        order1.setAddress(address1);
        order1.setOrder_ID(4);

        order2.setProducts(productList2);
        order2.setAddress(address1);
        order2.setOrder_ID(5);

        listOrders = new ArrayList<>();
        listOrders.add(order1);
        listOrders.add(order2);

        status1 = new Status("in transit");
        status1 = new Status("delivered");

        response1 = new JSONObject();
        response2 = new JSONObject();

        JSONObject data1 = new JSONObject();
        JSONObject data2 = new JSONObject();

        data1.put("order_id", order1.getOrder_ID());
        data1.put("status", "in transit");
        data2.put("order_id", order2.getOrder_ID());
        data2.put("status", "delivered");

        response1.put("data", data1);
        response2.put("data", data2);


    }

    @Test
    public void testIfStatusIsUpdated(){
        try {
            given(httpClient.getResponse("http://localhost:8086/order="+order1.getOrder_ID())).willReturn(response1);
            given(httpClient.getResponse("http://localhost:8086/order="+order2.getOrder_ID())).willReturn(response2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        given(orderRepository.findOrdersByUser_ID(any(Long.class))).willReturn(listOrders);
        given(statusRepository.findStatusByStatus_info("in transit")).willReturn(status1);
        given(statusRepository.findStatusByStatus_info("delivered")).willReturn(status2);
        service.fetchStatusOfOrders(1);
        verify(orderRepository, times(1)).save(order1);
        verify(orderRepository, times(1)).save(order2);

    }






}
