package com.ua.pt.FarmacySide.ServiceTests;

import com.ua.pt.FarmacySide.Forms.*;
import com.ua.pt.FarmacySide.Entities.User;
import com.ua.pt.FarmacySide.Repository.*;
import com.ua.pt.FarmacySide.Service.FarmacyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserRelated_Service_UnitTests {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FarmacyService service;

    User user1;

    LoginForm loginform;
    SignUpForm signUpForm;
    EditUsernameForm editUsernameForm;

    @BeforeEach
    public void setUp() {
        user1 = new User("mike123", "m.garcia.marques@ua.pt", "Miguel Marques", "1234", "912345678");

    }

    @Test
    public void testIfGivenANullUserResponse_TheServiceReturnsThatTheUserDoesntExists() {
        given(userRepository.findUserByUsername(any())).willReturn(null);

        loginform = new LoginForm("mike123", "1234");

        String response = service.checkIfUserRegistered(loginform);

        assertThat(response).isEqualTo("doesnt exist");
    }

    @Test
    public void testIfGivenAWrongPassword_TheServiceReturnsThatThePasswordIsWrong() {
        loginform = new LoginForm("mike123", "123");
        given(userRepository.findUserByUsername("mike123")).willReturn(user1);

        String response = service.checkIfUserRegistered(loginform);

        assertThat(response).isEqualTo("password wrong");
    }

   @Test
   public void testIfGivenARightPassword_TheServiceReturnsThatThePasswordIsRight() {
       loginform = new LoginForm("mike123", "1234");
       given(userRepository.findUserByUsername("mike123")).willReturn(user1);

       String response = service.checkIfUserRegistered(loginform);

       assertThat(response).isEqualTo("password right");
   }

    @Test
    public void testIfUserNameisUsed() {

        signUpForm = new SignUpForm("mike123", "m.garcia.marques@ua.pt", "1234", "1234", "Miguel Marques", "912345678");
        given(userRepository.findUserByUsername("mike123")).willReturn(user1);
        String response = service.saveUser(signUpForm);
        assertThat(response).isEqualTo("username used");

    }

    @Test
    public void testIfPasswordAreTheSame() {

        signUpForm = new SignUpForm("freddavoo", "fffs@ua.pt", "12345", "123456", "Miguel Marques", "912545678");

        String response = service.saveUser(signUpForm);

        assertThat(response).isEqualTo("password do not match");

    }

    @Test
    public void testsave() {

        signUpForm = new SignUpForm("joao2", "fffs@ua.pt", "12345678", "12345678", "Joao Monteiro", "932335678");

        String response = service.saveUser(signUpForm);

        assertThat(response).isEqualTo("user save");

    }

    @Test
    public void testIfUsernameGetsChanged(){

        EditUsernameForm editUsernameForm = new EditUsernameForm("mike1234");
        when(userRepository.findUserByUsername("mike1234")).thenReturn(null);
        boolean response = service.changeUsername(editUsernameForm, user1);

        assertThat(response).isEqualTo(true);
    }

    @Test
    public void testWhenUserDoesNotExistReturnFalse(){

        when(userRepository.findUserByUsername("mike1234")).thenReturn(user1);
        EditUsernameForm editUsernameForm = new EditUsernameForm("mike1234");
        boolean response = service.changeUsername(editUsernameForm, user1);

        assertThat(response).isEqualTo(false);
    }

    @Test
    public void testIfEmailGetsChanged() {
        EditMailForm editMailForm = new EditMailForm("mike@ua.pt");

        boolean response = service.changeMail(editMailForm, user1);

        assertThat(response).isEqualTo(true);
        verify(userRepository, times(1)).save(user1);
    }

    @Test
    public void testIfReturnNullWhenEmailISInvalid() {
        EditMailForm editMailForm = new EditMailForm("mike.pt");

        boolean response = service.changeMail(editMailForm, user1);

        assertThat(response).isEqualTo(false);
        verify(userRepository, times(0)).save(user1);
    }

    @Test
    public void testIfPasswordGetsChanged() {
        EditPasswordForm editPasswordForm = new EditPasswordForm("1234", "12345");

        boolean response = service.changePassword(editPasswordForm, user1);

        assertThat(response).isEqualTo(true);
        verify(userRepository, times(1)).save(user1);
    }

    @Test
    public void testIfReturnNullWhenOldPasswordsAreDifferent() {
        EditPasswordForm editPasswordForm = new EditPasswordForm("12345", "123456");

        boolean response = service.changePassword(editPasswordForm, user1);

        assertThat(response).isEqualTo(false);
        verify(userRepository, times(0)).save(user1);
    }


}
