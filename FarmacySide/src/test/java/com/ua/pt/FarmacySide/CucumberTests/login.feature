Feature: Login
  Background: Login into the Website
    Given I already have an account
  Scenario: Login Successfully
    When I fill correctly the login fields with username "mike123" and password "1234" and click the login button
    Then I should be logged in
  Scenario: Login failed with wrong username
    When I fill incorrectly the username field with "mike1234" and correctly the password field with "1234" and click the login button
    Then I should not be logged in
  Scenario: Login failed with wrong password
    When I fill correctly the username field with "mike1234" and incorrectly the password field with "1234" and click the login button
    Then I should also not be logged in