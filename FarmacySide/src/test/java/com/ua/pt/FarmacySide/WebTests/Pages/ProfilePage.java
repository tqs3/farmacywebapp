package com.ua.pt.FarmacySide.WebTests.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage {

    private WebDriver driver;

    @FindBy(how = How.CSS, using = ".info:nth-child(2) > .component:nth-child(2) > .content")
    private WebElement usernameText;

    @FindBy(how = How.CSS, using = ".info:nth-child(3) > .component:nth-child(1) > .content")
    private WebElement emailText;

    @FindBy(how = How.CSS, using = ".nav-item:nth-child(3) > .nav-link")
    private WebElement signOutButton;

    @FindBy(how = How.CSS, using = ".linkEdit:nth-child(6) > .edit")
    private WebElement editEmailButton;

    @FindBy(how = How.CSS, using = ".linkEdit:nth-child(4) > .edit")
    private WebElement editUsernameButton;

    @FindBy(how = How.CSS, using = ".linkEdit:nth-child(5) > .edit")
    private WebElement editPasswordButton;

    public ProfilePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getUsernameText() {
        return usernameText.getText();
    }

    public String getEmailText() {
        return emailText.getText();
    }

    public void clickSignOutButton() {
        signOutButton.click();
    }

    public void clickEditEmailButton() {
        editEmailButton.click();
    }

    public void clickEditUsernameButton() {
        editUsernameButton.click();
    }

    public void clickEditPasswordButton() {
        editPasswordButton.click();
    }
}
