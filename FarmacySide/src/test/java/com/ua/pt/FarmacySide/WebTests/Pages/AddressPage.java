package com.ua.pt.FarmacySide.WebTests.Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddressPage {

    private WebDriver driver;

    @FindBy(how = How.ID, using = "inserted_address")
    private WebElement addressField;

    @FindBy(how = How.ID, using = "inserted_zipcode")
    private WebElement zipcode1Field;

    @FindBy(how = How.ID, using = "inserted_zipcode")
    private WebElement zipcode2Field;

    @FindBy(how = How.ID, using = "inserted_city")
    private WebElement cityField;

    @FindBy(how = How.ID, using = "inserted_district")
    private WebElement districtField;

    @FindBy(how = How.ID, using = "inserted_country")
    private WebElement countryField;

    @FindBy(how = How.ID, using = "inserted_phonenumber")
    private WebElement phonenumber;





    @FindBy(how = How.ID, using = "btConfirm")
    private WebElement buttonConfirm;

    public AddressPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void insertInfoInAddressForm(String address, String zipcode1, String zipcode2, String city, String district, String country, String phonenumber) {
        addressField.click();
        addressField.sendKeys(address);

        zipcode1Field.click();
        zipcode1Field.sendKeys(zipcode1);

        zipcode2Field.click();
        zipcode2Field.sendKeys(zipcode2);

        zipcode2Field.click();
        zipcode2Field.sendKeys(city);

        zipcode2Field.click();
        zipcode2Field.sendKeys(district);

        zipcode2Field.click();
        zipcode2Field.sendKeys(country);

        zipcode2Field.click();
        zipcode2Field.sendKeys(phonenumber);
    }

    public void clickConfirmButton() {
        buttonConfirm.click();
    }
}
