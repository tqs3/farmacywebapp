package com.ua.pt.FarmacySide.WebTests.Pages;

import org.checkerframework.common.reflection.qual.ForName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    private WebDriver driver;

    @FindBy(how = How.ID, using = "uname")
    private WebElement usernameField;

    @FindBy(how = How.ID, using = "psw")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "btLogin")
    private WebElement buttonLogin;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void insertInfoInLoginForm(String username, String password) {
        usernameField.click();
        usernameField.sendKeys(username);

        passwordField.click();
        passwordField.sendKeys(password);
    }

    public void clickLoginButton() {
        buttonLogin.click();
    }
}
