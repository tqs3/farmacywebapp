package com.ua.pt.FarmacySide.CucumberTests;

import com.ua.pt.FarmacySide.Entities.User;
import com.ua.pt.FarmacySide.Forms.LoginForm;
import com.ua.pt.FarmacySide.Repository.UserRepository;
import com.ua.pt.FarmacySide.Service.FarmacyService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class LoginSteps {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FarmacyService service;

    private User user;
    private LoginForm loginForm;

    @Given("I already have an account")
    public void iAlreadyHaveAnAccount() {
        user = new User("mike123", "m.garcia.marques@ua.pt", "Miguel Marques", "1234", "912345678");
    }


    @When("I fill correctly the login fields with username {string} and password {string} and click the login button")
    public void iFillCorrectlyTheLoginFieldsWithUsernameAndPasswordAndClickTheLoginButton(String arg0, String arg1) {
        loginForm = new LoginForm(arg0, arg1);
    }

    @Then("I should be logged in")
    public void iShouldBeLoggedIn() {
        given(userRepository.findUserByUsername(loginForm.getUsername())).willReturn(user);
        String response = service.checkIfUserRegistered(loginForm);
        assertThat(response).isEqualTo("password right");
    }

    @When("I fill incorrectly the username field with {string} and correctly the password field with {string} and click the login button")
    public void iFillIncorrectlyTheUsernameFieldWithAndCorrectlyThePasswordFieldWithAndClickTheLoginButton(String arg0, String arg1) {
        loginForm = new LoginForm(arg0, arg1);
    }


    @Then("I should not be logged in")
    public void iShouldNotBeLoggedIn() {
        given(userRepository.findUserByUsername(loginForm.getUsername())).willReturn(null);
        String response = service.checkIfUserRegistered(loginForm);
        assertThat(response).isEqualTo("doesnt exist");
    }

    @When("I fill correctly the username field with {string} and incorrectly the password field with {string} and click the login button")
    public void iFillCorrectlyTheUsernameFieldWithAndIncorrectlyThePasswordFieldWithAndClickTheLoginButton(String arg0, String arg1) {
        loginForm = new LoginForm(arg0, arg1);
    }

    @Then("I should also not be logged in")
    public void iShouldAlsoNotBeLoggedIn() {
        given(userRepository.findUserByUsername(loginForm.getUsername())).willReturn(null);
        String response = service.checkIfUserRegistered(loginForm);
        assertThat(response).isEqualTo("password wrong");
    }
}
