package com.ua.pt.FarmacySide.RepositoryTests;

import com.ua.pt.FarmacySide.Entities.*;
import com.ua.pt.FarmacySide.Repository.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class OrderRepositoryTests {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;


    @Autowired
    private AddressRepository addressRepository;



    Orders order1;

    User user1;

    Product product1;
    Product product2;

    Address address1;

    List<Product> productList;

    @BeforeEach
    public void setUp() {
        user1 = userRepository.save(new User(
                "mike321",
                "mike321@ua.pt",
                "Miguel Marques",
                "4321",
                "918467367"));

        product1 = productRepository.findProductByProduct_ID(1);
        product2 = productRepository.findProductByProduct_ID(2);

        address1 = addressRepository.save(new Address(
                "Rua Prof José Carvalho nº870",
                3870,
                067,
                "Aveiro",
                "Aveiro",
                "Portugal",
                918567487,
                user1
        ));

        productList = new ArrayList<>();
        productList.add(product1);
        productList.add(product2);

        order1 = orderRepository.save(new Orders(0, new ArrayList<>(), user1));

        order1.setProducts(productList);
        order1.setAddress(address1);
        orderRepository.save(order1);
        addressRepository.save(address1);
    }

    @Test
    public void testFetchingOrderByUser_ID() {
        List<Orders> orders = orderRepository.findOrdersByUser_ID(user1.getUser_ID());

        for (Orders order : orders) {
            assertThat(order).isEqualTo(order1);
            assertThat(order.getUser_ID()).isEqualTo(user1);
        }
    }

    @Test
    public void testIfInformationWasSavedCorrectly() {
        List<Orders> orders = orderRepository.findOrdersByUser_ID(user1.getUser_ID());
        for (Orders order : orders) {
            assertThat(order.getAddress()).isEqualTo(address1);
            for (Product product : order.getProducts()) {
                assertThat(order.getProducts().get(0)).isEqualTo(order1.getProducts().get(0));
            }
        }
    }

    @Test
    public void testIfTotalPriceWasCalculatedAndSavedRight() {
        List<Orders> orders = orderRepository.findOrdersByUser_ID(user1.getUser_ID());
        float totalPrice = 0;
        for (Product p: productList) {
            totalPrice += p.getPrice();
        }
        for (Orders order : orders) {
            assertThat(order.getTotal_price()).isEqualTo(totalPrice);
        }
    }
}
