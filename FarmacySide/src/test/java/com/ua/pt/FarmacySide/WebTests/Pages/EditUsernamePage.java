package com.ua.pt.FarmacySide.WebTests.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class EditUsernamePage {

    private WebDriver driver;

    @FindBy(how = How.ID, using = "uname")
    private WebElement usernameField;

    @FindBy(how = How.ID, using = "btChange")
    private WebElement buttonChange;

    public EditUsernamePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickChangeButton() {
        buttonChange.click();
    }

    public void insertUsernameInfoInForm(String username) {
        usernameField.click();
        usernameField.sendKeys(username);
    }
}
