package com.ua.pt.FarmacySide.CucumberTests;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.cucumber.junit.platform.engine.Cucumber;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

@Cucumber
public class LoginCucumberTest {

    @TestFactory
    public Stream<DynamicTest> runCukes(Stream<DynamicTest> scenarios) {
        List<DynamicTest> tests = scenarios.collect(Collectors.toList());
        return tests.stream();
    }

}
