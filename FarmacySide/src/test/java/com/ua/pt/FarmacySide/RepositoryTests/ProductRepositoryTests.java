package com.ua.pt.FarmacySide.RepositoryTests;

import com.ua.pt.FarmacySide.Entities.Product;
import com.ua.pt.FarmacySide.Repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ProductRepositoryTests {

    @Autowired
    private ProductRepository productRepository;

    Product product1;

    Product product2;

    @BeforeEach
    public void setUp() {

        product1 = productRepository.save(new Product(
                "Benurom 1000",
                "Benurom 1000mg",
                15,
                (float) 5.76,
                "/db_images/benurom.jpeg"
        ));

        product2 = productRepository.save(new Product(
                "Aerius 5mg",
                "Aerius 5mg",
                10,
                (float) 2.46,
                "/db_images/aerius5mg.jpeg"
        ));

    }

    @Test
    public void testIfItFetchesTheCorrectProduct() {
        long id = productRepository.findIdLastProduct();
        Product product = productRepository.findProductByProduct_ID(id);

        assertThat(product).isEqualTo(product2);
    }

    @Test
    public void testIfInfoOfProductISStoredCorrectly() {
        long id = productRepository.findIdLastProduct();
        Product product = productRepository.findProductByProduct_ID(id);

        assertThat(product.getProduct_name()).isEqualTo(product2.getProduct_name());
        assertThat(product.getProduct_ID()).isEqualTo(product2.getProduct_ID());
        assertThat(product.getInfo()).isEqualTo(product2.getInfo());
        assertThat(product.getPrice()).isEqualTo(product2.getPrice());
    }
}
